import React from 'react';
import  s from './preloader.scss'
const Preloader = () => {
    return <div className={s.loader}></div>;
}

export default Preloader;

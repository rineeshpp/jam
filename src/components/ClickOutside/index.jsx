import React, { useRef } from 'react';
import useOutsideClick from '@/hooks';
import PropTypes from 'prop-types';
const ClickOutside = ({ children, callbackHandler, ...rest }) => {
  const wrapperRef = useRef(null);

  useOutsideClick(wrapperRef, callbackHandler);
  return (
    <div ref={wrapperRef} {...rest}>
      {children}
    </div>
  );
};
ClickOutside.propTypes = {
  children: PropTypes.element.isRequired,
  callbackHandler: PropTypes.func.isRequired,
};
export default ClickOutside;

import React from 'react';

import s from './index.scss';

function Checkbox({className, id, checked=false, onChange = () => {},children },{...rest}) {

  return (
    <div className={`${s.container} ${className}`} {...rest}>
      <input
        id={id}
        className={s.element}
        type="checkbox"
        checked={checked}
        onClick={onChange}
        readOnly
      />
      <label htmlFor={id}  role="presentation" >{children}</label>
    </div>
  );
}

Checkbox.displayName = 'Checkbox';

export default Checkbox;

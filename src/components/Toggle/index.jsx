import React, { useState } from 'react';
import filter from 'lodash/filter';
import isEqual from 'lodash/isEqual';
import s from './toggle.scss';

const Toggle = (props) => {
  const { onClickCb, mode, options, activeId, showLabel } = props;
  const firstItem = options[0];
  const secondItem = options[1];
  const [activeItemId, setActiveItem] = useState(activeId);
  const onclickHandler = () => {
    const activeItem = filter(
      options,
      (item) => !isEqual(item.id, activeItemId)
    );
    onClickCb(activeItem[0]);
    setActiveItem(activeItem[0].id);
  };
  return (
    <div className={s.SwitchBoxWrapper}>
      <div
        role="presentation"
        className={`${s.SwitchBox} ${
          isEqual(secondItem.id, activeItemId) ? s.on_right : s.on_left
        }`}
        onClick={onclickHandler}>
        {showLabel && (
          <span
            key="label1"
            className={`${
              isEqual(firstItem.id, activeItemId) ? s.active : ''
            } ${s.labelLeft}`}>
            {firstItem.label}
          </span>
        )}

        <div
          className={`${s.Toggle} ${
            isEqual(mode, 'toggle') ? s.toggle : s.switch
          }`}
        />

        {showLabel && (
          <span
            key="label2"
            className={`${
              isEqual(secondItem.id, activeItemId) ? s.active : ''
            } ${s.labelRight}`}>
            {secondItem.label}
          </span>
        )}
      </div>
    </div>
  );
};
Toggle.defaultProps = {
  showLabel: true,
  options: '',
  activeId: String,
  mode: 'switch', // 'toggle',
};
export default Toggle;

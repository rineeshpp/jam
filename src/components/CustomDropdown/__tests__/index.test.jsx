import React from 'react';
import { shallow, mount } from 'enzyme';
import toJson from 'enzyme-to-json';

import CustomDropdown from '../index';

const commonProps = {
  selectedValue: {
    label: 'Financial Year 2018-19',
    value: 2,
  },
  options: [
    {
      label: 'Financial Year 2018-19',
      value: 2,
    },
    {
      label: 'Financial Year 2018-19',
      value: 19,
    },
    {
      label: 'Custom',
      value: 18,
    },
  ],
  onChange: () => {},
  containerClass: 'NX-NEb6iVhNWf17n2RhW9',
};

describe('Custom Dropdown Snapshot tests', () => {
  it('render', () => {
    const tree = shallow(<CustomDropdown {...commonProps} />);
    expect(toJson(tree)).toMatchSnapshot();
  });

  it('dropdown open', () => {
    const tree = shallow(<CustomDropdown {...commonProps} />);
    tree.find('#dropdown-control').simulate('click');
    expect(toJson(tree)).toMatchSnapshot();
  });

  it('close dropdown on click on select-control', () => {
    const tree = shallow(<CustomDropdown {...commonProps} />);
    tree.find('#dropdown-control').simulate('click');
    tree.find('#dropdown-control').simulate('click');
    expect(toJson(tree)).toMatchSnapshot();
  });
});

describe('Custom Dropdown functional tests', () => {
  it('option selected', () => {
    const spy = jest.fn();
    const { options } = commonProps;
    const tree = shallow(<CustomDropdown {...commonProps} onChange={spy} />);
    tree.find('#dropdown-control').simulate('click');
    tree.find('.dropdown').find('div').last().simulate('click');
    expect(spy).toHaveBeenCalledWith(options[options.length - 1]);
  });

  it('click event tests', () => {
    const events = {};
    jest
      .spyOn(document, 'addEventListener')
      .mockImplementation((event, handle) => {
        events[event] = handle;
      });
    jest.spyOn(document, 'removeEventListener').mockImplementation((event) => {
      events[event] = undefined;
    });

    const wrapper = mount(<CustomDropdown {...commonProps} />);
    events.mousedown({ target: null });
    expect(document.addEventListener).toBeCalledWith(
      'mousedown',
      expect.any(Function)
    );
    wrapper.unmount();
    expect(document.removeEventListener).toBeCalledWith(
      'mousedown',
      expect.any(Function)
    );
  });

  it('click event when dropdown is open', () => {
    const events = {};
    jest
      .spyOn(document, 'addEventListener')
      .mockImplementation((event, handle) => {
        events[event] = handle;
      });
    jest.spyOn(document, 'removeEventListener').mockImplementation((event) => {
      events[event] = undefined;
    });

    const wrapper = mount(<CustomDropdown {...commonProps} />);
    wrapper.find('#dropdown-control').simulate('click');
    events.mousedown({ target: null });
    expect(document.addEventListener).toBeCalledWith(
      'mousedown',
      expect.any(Function)
    );
  });
});

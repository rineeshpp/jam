import React, { useState, useRef, useEffect } from 'react';
import PropTypes from 'prop-types';

import { classNames as cx } from '@utils/style';
import Icon from '../Icon';
import { ICON_NAME } from '../Icon/enums';
import selectStyles from './index.scss';

const CustomDropdown = ({
  selectedValue,
  options,
  onChange,
  containerClass,
  optionDefaultClass,
  displayValue = null,
}) => {
  const wrapperRef = useRef(null);
  const [isDropdownOpen, setIsDropdownOpen] = useState(false);

  const openDropdown = (event) => {
    event.stopPropagation();
    if (isDropdownOpen) {
      setIsDropdownOpen(false);
    } else {
      setIsDropdownOpen(true);
    }
  };

  const hideDropdown = (selectedOption) => {
    setIsDropdownOpen(false);
    onChange(selectedOption);
  };

  const handleClickOutside = (event) => {
    if (wrapperRef.current && !wrapperRef.current.contains(event.target)) {
      if (isDropdownOpen) {
        setIsDropdownOpen(false);
      }
    }
  };

  useEffect(() => {
    document.addEventListener('mousedown', handleClickOutside);
    return () => {
      document.removeEventListener('mousedown', handleClickOutside);
    };
  });

  const getContainerStyles = () =>
    cx([selectStyles.container, containerClass], {
      [selectStyles.dropdownOpen]: isDropdownOpen,
    });

  const iconName = isDropdownOpen ? ICON_NAME.DROP_UP : ICON_NAME.DROP_DOWN;

  const getIconClass = () =>
    cx('', {
      [selectStyles.iconClass]: !isDropdownOpen,
    });

  return (
    <>
      <div ref={wrapperRef}>
        <div
          id="dropdown-control"
          role="presentation"
          className={getContainerStyles()}
          onClick={openDropdown}>
          <div>{displayValue}</div>
          <div className={selectStyles.iconContainer}>
            <Icon name={iconName} className={getIconClass()} />
          </div>

          {isDropdownOpen && (
            <div className={selectStyles.dropdown}>
              {options.map((option) => {
                const optionClass =
                  option.id === selectedValue.id
                    ? selectStyles.selectedOption
                    : '';
                return (
                  <div
                    role="presentation"
                    key={option.id}
                    className={`${optionDefaultClass} ${optionClass}`}
                    onClick={() => hideDropdown(option)}>
                    {option.label}
                  </div>
                );
              })}
            </div>
          )}
        </div>
      </div>
    </>
  );
};

CustomDropdown.propTypes = {
  selectedValue: PropTypes.oneOfType([PropTypes.object]),
  options: PropTypes.arrayOf(PropTypes.object).isRequired,
  onChange: PropTypes.func.isRequired,
  containerClass: PropTypes.string,
};

CustomDropdown.defaultProps = {
  containerClass: '',
};

export default CustomDropdown;

import React from 'react';
import s from './items.scss';
const Items = ({ content, imageUrl, className }) => {
  return (
    <div className={`${className} ${s.item}`}>
      <img src={imageUrl}></img>
      <div dangerouslySetInnerHTML={{ __html: content }}></div>
    </div>
  );
};

export default Items;

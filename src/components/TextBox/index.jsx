import React from 'react';
import s from './textbox.scss';

const TextBox = (props) => {
  const {
    errMsg,
    label,
    name,
    type = 'text',
    placeHolder,
    showError = false,
    hint,
    isOptional = false,
    className,
    maxLength,
    min,
    max,
    handleKeyDown,
    handleOnChange,
    tabIndex,
    refId,
    value,
  } = props;
  return (
    <fieldset
      className={`${s.InputTextBox} ${!!showError ? s.error : null} 
      ${className ? className : null}`}>
      {label && (
        <label htmlFor={refId}>
          {label}
          {isOptional && <span className={s.optional}>{optional}</span>}
        </label>
      )}
      <input
        onKeyDown={handleKeyDown}
        onChange={handleOnChange}
        type={type}
        name={name}
        placeholder={placeHolder}
        maxLength={maxLength}
        min={min}
        max={max}
        tabIndex={tabIndex}
        value={value}
        ref={refId}
      />
      {hint && <span className={s.hint}>{hint}</span>}
      {showError && <span className={s.error_message}>{errMsg}</span>}
    </fieldset>
  );
};
export default TextBox;

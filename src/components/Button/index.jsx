import React from 'react';
import s from './button';
const Button = ({ onClick, children, secondory, className }) => {
  return (
    <button
      className={`${s.button} ${secondory ? s.secondory : null} ${
        className ? className : null
      }`}
      onClick={onClick}>
      <div className={`}`}>{children}</div>
    </button>
  );
};
export default Button;

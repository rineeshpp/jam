import React, { useState } from 'react';
import ClickOutside from '../ClickOutside';
import PropTypes from 'prop-types';
import s from './selectbox';

const SelectBox = ({
  label,
  placeHolder,
  className,
  refId,
  isOptional,
  options,
  selected,
  onChange,
  showError,
  hint,
}) => {
  const [active, setActive] = useState(false);
  const onClickHandler = (item, cb) => {
    cb(item);
    setActive(false);
  };
  return (
    <fieldset
      className={`${s.select_box} ${!!showError ? s.error : null} 
      ${className ? className : null}`}>
      {label && (
        <label htmlFor={refId}>
          {label}
          {isOptional && <span className={s.optional}>{optional}</span>}
        </label>
      )}
      {
        <ClickOutside
          callbackHandler={() => setActive(false)}
          className={`${s.option_wrapper} ${active ? s.active : ''}`}
          onClick={() => setActive(!active)}>
          <>
            <span className={`${selected?.name ? s.selectedVal : ''}`}>
              {selected?.name || placeHolder || 'Select'}
            </span>
            <i className="icon-chevron-down"></i>
            <div className={s.options}>
              <ul>
                {options?.map((item) => {
                  return (
                    <li
                      key={item.value}
                      onClick={() => onClickHandler(item, onChange)}>
                      {item.name}
                    </li>
                  );
                })}
              </ul>
            </div>
          </>
        </ClickOutside>
      }
      {hint && <span className={s.hint}>{hint}</span>}
      {showError && <span className={s.error_message}>{errMsg}</span>}
    </fieldset>
  );
};

SelectBox.propTypes = {
  placeHolder: PropTypes.string,
  options: PropTypes.array,
  selected: PropTypes.object,
  onChange: PropTypes.func,
  label: PropTypes.string,
  className: PropTypes.string,
  refId: PropTypes.string,
  isOptional: PropTypes.bool,
};
export default SelectBox;

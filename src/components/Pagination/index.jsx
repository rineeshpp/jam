import React, { useState, useEffect } from 'react';
import PropTypes from 'prop-types';
import s from './index.scss';

const Pagination = ({
  pageSize,
  totalSize,
  activePage = 1,
  limit = 5,
  onChange,
}) => {
  const [active, setAct] = useState(activePage);
  const totalPages = getTotalPages(pageSize, totalSize);
  useEffect(() => {
    onChange(active);
  }, [active]);
  return (
    <ul className={s.pagination}>
      <li onClick={() => clickPrev(setAct, active, totalPages)}>
        <i className="icon-chevron-left"></i>
      </li>
      {list(totalPages, active, limit, setAct).map((item) => {
        return (
          <li
            key={item}
            className={`${active == item ? s.active : ''}`}
            onClick={() => setAct(item)}>
            {item}
          </li>
        );
      })}
      <li onClick={() => clickNext(setAct, active, totalPages)}>
        <i className="icon-chevron-right"></i>
      </li>
    </ul>
  );
};

const list = (num, act, lim) => {
  let lst = [],
    max,
    srt;
  let dif = Math.floor(lim / 2);
  srt = act - dif <= 1 ? 1 : act - dif;
  max = srt + lim - 1;
  if (max >= num) {
    max = num;
  }
  for (srt; srt <= max; srt++) {
    lst.push(srt);
  }
  return lst;
};
const clickPrev = (cb, act) => {
  act = act - 1 <= 1 ? 1 : act - 1;
  cb(act);
};
const clickNext = (cb, act, ttl) => {
  act = act + 1 >= ttl ? ttl : act + 1;
  cb(act);
};
const getTotalPages = (pageSize, totalSize) => {
  totalSize = totalSize ? parseInt(totalSize) : 0;
  pageSize = pageSize ? parseInt(pageSize) : 16;
  let count = Math.ceil(totalSize / pageSize);
  count <= 0 ? 1 : count;
  return count;
};
Pagination.propTypes = {
  pageSize: PropTypes.number.isRequired,
  totalSize: PropTypes.number.isRequired,
  onChange: PropTypes.func.isRequired,
};

export default Pagination;

import React from 'react';
import PropTypes from 'prop-types';
import styles from './index.scss';

function Switch({ options = [], active = '', setSwitch = () => {} }) {
  return (
    <div className={styles.container}>
      {options.length &&
        options.map((item) => (
          <div
            className={`${item.id === active ? styles.active : ''}`}
            key={item.id}
            onClick={() => setSwitch(item.id)}
            role="presentation">
            {item.label}
          </div>
        ))}
    </div>
  );
}

Switch.propTypes = {
  options: PropTypes.arrayOf(Object).isRequired,
  active: PropTypes.string.isRequired,
  setSwitch: PropTypes.func.isRequired,
};

export default Switch;

import { useEffect } from 'react';
const useOutsideClick = (ref, cb) => {
  useEffect(() => {
    const handleclickOutside = (event) => {
      if (ref.current && !ref.current.contains(event.target)) {
        cb();
      }
    };
    document.addEventListener('mousedown', handleclickOutside);
    return () => {
      document.removeEventListener('mousedown', handleclickOutside);
    };
  }, [ref]);
};
export default useOutsideClick;

import axios from "axios";
const CancelToken = axios.CancelToken;
const clientRequest = options =>
  new Promise((resolve, reject) => {
    const source = new CancelToken.source();
    const customHeaders = setHeaders();
    options.headers = { ...options.headers, ...customHeaders };
    options.cancelToken = source.token;
    var request = axios.create();
    request(options, {
      cancelToken: source.token
    })
      .then(res => {
        resolve(res);
      })
      .catch(errorResp => {
        reject(errorResp);
      });
  });

const setHeaders = () => {
 return {
   'x-rapidapi-host': 'imdb8.p.rapidapi.com',
   'x-rapidapi-key': '8e8085b151msh3046eb2fe52a102p1d38ebjsne831a4c3b283',
 };
};

export default {
  get: options => clientRequest(options),
  post: options => {
    const requestOptions = {
      method: "POST",
      ...options
    };
    return clientRequest(requestOptions);
  },

  put: options => {
    const requestOptions = {
      method: "PUT",
      ...options
    };
    return clientRequest(requestOptions);
  }
};

import React from 'react';
import s from './home.scss';
import Banner from './Banner';
import HpContent from './HpContent';

import Items from '@/components/Items';
import content from '@/content/portfolio/item1.md';
const Home = () => (
  <div className={s.home_wrapper}>
    {
      <>
        <Banner></Banner>
        <HpContent>
        <div className="row">
          <Items
            className="col-xs-6 col-md-4"
            content={content}
            imageUrl={require(`@/assets/images/works/img-01.png`)}></Items>
          <Items
            className="col-xs-6 col-md-4"
            content={content}
            imageUrl={require(`@/assets/images/works/img-02.png`)}></Items>
          <Items
            className="col-xs-6 col-md-4"
            content={content}
            imageUrl={require(`@/assets/images/works/img-03.png`)}></Items>
            </div>
        </HpContent>
      </>
    }
  </div>
);
export default Home;

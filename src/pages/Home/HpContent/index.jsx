import React from 'react';
import s from './hp.scss';
import content from '@/content/home/hp.md';
const HpContent = ({children}) => {
  return (
    <div className="container">
      <div className="row">
        <div className="col-xs">
          <section className={s.content}>
            <div className={s.title} dangerouslySetInnerHTML={{ __html: content }}></div>
            {children}
          </section>
        </div>
      </div>
    </div>
  );
};
export default HpContent;

import React from 'react';
import s from './banner.scss';

const Banner = () => {
  return (
    <section className={s.bannerbg}>
      <div className="container">
        <div className={s.banner_text}>
          <h1>Lorem ipsum dolor sit amet</h1>
          <p>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Phasellus
            pellentesque posuere nulla. Pellentesque eget lobortis quam, ac{' '}
            <br></br>
            varius dolor. Quisque a lacus in arcu maximus sollicitudin ac nec
            est.
          </p>
        </div>
      </div>
    </section>
  );
};
export default Banner;

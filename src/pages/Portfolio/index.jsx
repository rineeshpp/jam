import React from 'react';
import  marked from 'marked';
import PropTypes from 'prop-types';
import s from './portfolio'
import Items from '@/components/Items';
import content from '@/content/portfolio/item1.md';
const Portfolio = () => {
   const items=new Array(6).fill(1);
    return (
      <div className={`container ${s.wrapper}`}>
        <div className="row">
          {items.map((item, i) => {
            return (
              <Items
                key={i}
                className="col-xs-6 col-md-4"
                content={content}
                imageUrl={require(`@/assets/images/works/img-0${
                  i + 1
                }.png`)}></Items>
            );
          })}
        </div>
      </div>
    );
};


Portfolio.propTypes = {

};


export default Portfolio;

import React, { useEffect,useState,useRef } from 'react';
import Preloader from '@/components/Preloader';
import PropTypes from 'prop-types';
import ApiService from '@/services/apiService'
import s from './service'
const Services = () => {
    const [data,setData]= useState([]);
    const isInitialMount = useRef(true);
    const [isLoading,setLoading]= useState(true);
    const getData = async () => {
      
      let response = await ApiService.get({
        url:
          'https://imdb8.p.rapidapi.com/title/get-news?limit=8&tconst=tt0944947',
      });
      let {data} = response;
      setData({...data});
    };
    useEffect(() => {
        getData();
        setLoading(true);
    }, []);
    useEffect(() => {
        if (isInitialMount.current) {
          isInitialMount.current = false;
        }else{
            setLoading(false);
        }
      
    }, [data]);
    
    
    return (
      <div className="container">
        <div className={s.services_wrapper}>
          <div className="row">
            {isLoading&&<div className={s.preloader_wrapper}><Preloader/></div>}
            <div className="col-xs">
              <h1>{data?.title}</h1>
            </div>
          </div>
          <div className="row">
            {data?.items?.map((item, i) => {
              return (
                <div key={i} className="col-xs-6">
                  <div className={s.newslist}>
                    <figure>
                      <img src={item.image.url} />
                    </figure>
                    <div className={s.desc}>
                      <h3>{item.head}</h3>
                      <span>{item.publishDateTime}</span>
                      <p>{item.body}</p>
                    </div>
                  </div>
                </div>
              );
            })}
          </div>
        </div>
      </div>
    );
};


Services.propTypes = {

};


export default Services;

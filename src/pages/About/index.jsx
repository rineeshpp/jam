import React from 'react';
import PropTypes from 'prop-types';
import content from '@/content/about/aboutus.md';
import s from './about.scss'
const About = () => {
    return (
      <div className="container">
        <div className="row">
          <div className="col-xs">
            <div className={s.about_content} dangerouslySetInnerHTML={{ __html: content }}></div>
          </div>
        </div>
      </div>
    );
};


About.propTypes = {

};


export default About;

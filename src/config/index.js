import development from './development';
import production from './production';
const ENV_CONFIG = {
  development,
  production,
};
const ENV =
  process.env.NODE_ENV && ENV_CONFIG[process.env.NODE_ENV]
    ? process.env.NODE_ENV
    : 'development';

if (ENV !== process.env.NODE_ENV) {
  console.warn(
    `No config available for '${process.env.NODE_ENV}' environment. Using '${ENV}' environment instead.`
  );
}
export const { API_CONF, BASE_PATH, HOST } = ENV_CONFIG[ENV];

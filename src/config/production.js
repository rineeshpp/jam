const API_HOST = `https://marriageparties.com`;

const BASE_PATH = {
  api: `${API_HOST}/MarriageParties/api`,
  otherApi: `${API_HOST}/other/path/api`,
};

export const API_CONF = {
  popularSearchUrl: `${BASE_PATH.api}/getpopularvenuesearches`,
};

const apiConfig = {
  API_CONF,
  BASE_PATH,
  HOST: `https://marriageparties.com`,
};

export default { ...apiConfig };

const API_HOST = `http://localhost`;

const BASE_PATH = {
  api: `${API_HOST}/MarriageParties/api`,
  otherApi: `${API_HOST}/other/path/api`,
};

export const API_CONF = {
  popularSearchUrl: `${BASE_PATH.api}/getpopularvenuesearches`,
  getLocationsUrl: `${BASE_PATH.api}/getlocations`,
  getCategoriesUrl: `${BASE_PATH.api}/getcategories`,
  getVendorListUrl: `${BASE_PATH.api}/getvendorlist`,
};

const apiConfig = {
  API_CONF,
  BASE_PATH,
  HOST: `http://localhost`,
};

export default { ...apiConfig };

/* eslint-disable */
module.exports = (env) => {
  const { NODE_ENV } = process.env;
  return require(`./webpack.${NODE_ENV}.js`);
};
